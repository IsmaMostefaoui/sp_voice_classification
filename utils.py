# -*- coding: utf-8 -*

from matplotlib.pyplot import xcorr
import matplotlib.pyplot as plt
from scipy.io import wavfile
import numpy as np
from scipy.signal import freqz
from scipy.fftpack import fft, ifft, dct
from random import randint
from filterbanks import *
from scipy.signal import filtfilt,freqz,periodogram

#supposed that array_signal is different than an array full of zeros
def normalize(array_signal):
	'''
	Normalise un tableau de nombre
	param: array_signal: le signal à normaliser sous forme de vecteur
	return: un tableau ou tous les nombres ont été divisé par le max
			un tableau ou min = -1 et max = 1
	'''
	maximum = float(max(map(abs, array_signal)))
	return [x / maximum for x in array_signal]


def split(array_signal, width, step):
	'''
	Sépare un signal en plusieurs frame de taille width décalé de step
	param: 	array_signal: le signal sous forme d'un vecteur
			width: la largeur des frames du signal (en echantillons pas ms)
			step: la décalage avec lequel on sépare le signal en morceau (échantillons pas ms)
	'''
	splitted_signal = []
	size = len(array_signal)
	width = int(width)
	step = int(step)
	for x in range(width+(size/step)):
		i=x*step
		if (i+width<size):
			splitted_signal.append(array_signal[i:i+width])
	return splitted_signal

#return the energy of the signal sig
def energy(sig):
	'''
	param: sig: le signal sous forme d'un vecteur
	return: l'energie du signal
	'''
	return sum(map(lambda x : abs(x)**2, sig))

#return a tab of energy associated to a frame
def energy_tab(sig):
	'''
	param: sig: le signal split sous forme de vecteur 1xN (n nombre de frame)
	return: un tableau, l'index i contient l'energie de la frame i-1 comprise dans sig
	'''
	energy_tab=[]
	for tab in sig:
		energy_tab.append(energy(tab))
	return energy_tab

#return 10% of the max energy of a tab of frames
#c.f. http://support.ircam.fr/docs/AudioSculpt/3.0/co/VoicedUnvoiced%20Analysis.html
def tresh(tab_of_sig):
	return 10 #beaucoup moins couteux
	#return 1*max([energy(sig) for sig in tab_of_sig])/10.

def is_voiced(sig, treshold):
	'''
	Vérifie si un signal est voiced ou unvoiced
	param: 	sig: le signal sous forme d'un vecteur
			treshold: le seuil d'énergie permettant de définir un signal voiced
	return true si le signal est voiced, false sinon
	'''
	return True if energy(sig) > treshold else False

#return a tab of voiced/unvoiced 
def determination(energy_tab, treshold):
	'''
	Crée un tableau de 'voiced'/'unvoiced' élements sur base des frames du signal
	param: 	energy_tab: le signal split sous forme de vecteur 1xN (n nombre de frame)
			treshold: le seuil d'énergie permettant de définir un signal voiced
	return: un tableau de 'voiced'/'unvoiced'
	'''
	voiced = []
	for i in range(len(energy_tab)):
		if energy_tab[i] > treshold:
			voiced.append('voiced')
		else:
			voiced.append('unvoiced')
	return voiced

#return a tab of unvoiced sig 
def sort_unvoiced(tab_of_sig):
	'''
	param: 	tab_of_sig: le signal split sous forme de vecteur 1xN (n nombre de frame)
	return un tableau comoposé de toutes les frames voiced de tab_of_sig
	'''
	treshold = tresh(tab_of_sig)
	return list(filter(lambda sig : not(is_voiced(sig, treshold)), tab_of_sig))

#return a tab of voiced sig
def sort_voiced(tab_of_sig):
	'''
	param: 	tab_of_sig: le signal split sous forme de vecteur 1xN (n nombre de frame)
	return un tableau comoposé de toutes les frames unvoced de tab_of_sig
	'''
	treshold = tresh(tab_of_sig)
	print(treshold)
	return list(filter(lambda sig : is_voiced(sig, treshold), tab_of_sig))


#supposed that first max is on maxlags (always true if array is the result of xcorr) 
def next_max(xcorr_sig, maxlags):
	'''
	Trouve le prochain maximum dan xcorr_sig
	param:	xcorr_sig: le signal résultant de la méthode xcorr (deuxième paramètre)
			maxlags: le paramètre maxlags donné à la méthode xcorr
	return: l'indice du prochain maximum
			la valeur du prochain maximum
	'''
	max_sig = next_min(xcorr_sig, maxlags)[1]
	max_sig_index = next_min(xcorr_sig, maxlags)[0]
	for i in range(max_sig_index,len(xcorr_sig)):
		if (xcorr_sig[i] > max_sig):
			max_sig = xcorr_sig[i]
			max_sig_index = i
	return (max_sig_index, max_sig)

#return the first minimum in the graph xcorr
#index is where to begin the search
#return (index of min, min)
def next_min(xcorr_sig,index=0):
	'''
	Trouve le prochain minimum dan xcorr_sig
	param:	xcorr_sig: le signal résultant de la méthode xcorr (deuxième paramètre)
			index: l'index du premier maximum du vecteur résultant de xcorr (deuxème param)
	return: l'indice du prochain minimum
			la valeur du prochain minimum
	'''
	start = 50
	for i in range(index+start,len(xcorr_sig)-1):
		if xcorr_sig[i]<xcorr_sig[i+1]:
			return i,xcorr_sig[i]

def compute_max_dist(xcorr_sig, maxlags):
	'''
	Calcule la distance entre le premier maximum et le deuxième dans le vecteur
	xcorr_sig
	param:	xcorr_sig: le signal résultant de la méthode xcorr (deuxième paramètre)
			maxlags: le paramètre maxlags donné à la méthode xcorr
	return: la distance entre les deux premiers maximum
	'''
	return next_max(xcorr_sig, maxlags)[0] - maxlags

def frame_f0(xcorr_sig, fe, maxlags):
	'''
	Calcule la fréquence fondamentale d'un signal sur base de sa fréquence d'échantillonage
	param:	xcorr_sig: le signal résultant de la méthode xcorr (deuxième paramètre)
			fe: la fréquence d'échantillonage du signal
			maxlags: le paramètre maxlags donné à la méthode xcorr
	'''
	dist = compute_max_dist(xcorr_sig, maxlags)
	return float(fe)/dist

def compute_f0(framed_signal, fe, maxlag):
	'''
	calcule toutes les fréquences fondamentales contenues dans le vecteur composé de frame
	param: 	framed_signal: un vecteur 1xN de frame
			fe: la fréquence d'échantillonage du signal
			maxlag: le paramètre maxlags donné à la méthode xcorr
	'''
	l=[]
	for frame in framed_signal:
		if is_voiced(frame, tresh(framed_signal)):
			_, c, _, _ = xcorr(frame, frame, maxlags=maxlag)
			f0 = frame_f0(c, fe, maxlag)
			if (f0 > 600):
				print('ca depasse l humain')
			l.append(f0)
		else:
			l.append(0)
	return l

def compute_f0_cepstrum(framed_signal, fe):
	'''
	Calcule la fréquence fondamentale de toutes les frames de framed_signal
	selon la methodé cepstrum.
	param: 	framed_signal: un vecteur 1xN de frame
			fe: la fréquence d'échantillonage du signal
	'''
	l=[]
	start_frequency = 50.
	end_frequency = 600.
	for frame in framed_signal:
		if is_voiced(frame, tresh(framed_signal)):
			# z = np.fft.ifft(frame)
			# w = np.fft.fftfreq(int(width), 1./fe)	
			w, z=freqz(frame, 1, worN = 2048)
			z = 20*np.log10(abs(z))
			# z = abs(z)
			# w = w[:len(w)/2]
			# z = z[:len(z)/2]
			w = w*fe/(2.*np.pi)
			#règle de trois pour trouver l'endroit ou commencer la recherche
			#du peak dans les cepstrum coeffcient
			start = int((len(w)/w[len(w)-1])*start_frequency)
			end = int((len(w)/w[len(w)-1])*end_frequency)
			peak = z[start]
			peak_index = w[start]
			for i in range(len(z)):
				if i >= start and i <= end:
					if z[i] > peak:
						peak_index = w[i]
						peak = z[i]
			l.append(peak_index)
		else:
			l.append(0)
	return l

def voice_chooser():
	'''
	Chosit aléatoirement 5 fichiers dans le dossier sound
	Ajoute les fréquence d'échantillonage et le signal à la liste retournée.
	param: /
	return: la liste de tuples (fe, signal) des 5 signaux choisi
	'''
	l=[]
	for i in range(5):
		x = str(randint(1, 541))
		if len(x) == 1:
			file = 'sound/arctic_a000' + x
		elif len(x) == 2:
			file = 'sound/arctic_a00' + x
		else:
			file = 'sound/arctic_a0' + x
		l.append(wavfile.read(file + '.wav'))
	return l

def plot_signal(fe, signal):
	width = 0.05*fe
	step = 0.01*fe
	framed_signal = split(normalize(signal), width, step)
	#plt.figure()
	# todo energy calculation + plot
	energy=energy_tab(framed_signal)
	t_energy=range(0,len(signal),len(signal)/len(energy))

	fig,ax1=plt.subplots()

	ax1.plot(signal)
	plt.ylim((int(-max(signal)-50),int(max(signal)+50)))

	ax2=ax1.twinx()

	ax2.plot(t_energy[:len(energy)],energy,color='r')
	plt.ylim((int(-max(energy)-5),int(max(energy)+5)))
	fig.tight_layout()

def high_pass_filter(framed_signal,alpha):
	l=[]
	for signal in framed_signal:
		signal=hpf(signal,alpha)
		l.append(signal)
	return l

def hpf(signal,alpha):
	b=np.asarray([1,-alpha])
	a=np.asarray([1])
	return np.hamming(len(signal))*filtfilt(b,a,signal)

def plot_alot(alot):
	for fe, signal in alot:
		plot_signal(fe, signal)
	plt.show()

def mfcc(sig,width,step,fe):
	a=0.97
	b=np.asarray([1,a])
	a=np.asarray([1])
	preesig=filtfilt(b,a,sig)

	framed_signal=split(preesig,width,step)

	fr_s=[]
	for frame in framed_signal:
		fr_s.append(np.hamming(len(frame))*frame)
	fr_s=np.asarray(fr_s)

	Ndft=512
	f,p=periodogram(fr_s,fe,nfft=Ndft)

	power=np.asarray(p)

	k=filter_banks(power,fe)

	res=dct(k,type=2,axis=1,norm='ortho')

	return res[0:13]
	


if __name__ == '__main__':

	fe, sig = wavfile.read('sound/arctic_a0001.wav')
	sig = normalize(sig)
	width = 0.05*fe
	step = 0.01*fe
	framed_signal = split(sig, width, step)
	# print(len(framed_signal))
	# voiced_signal = sort_voiced(framed_signal)
	#print(len(voiced_signal[0]))
	#_, c, _, _ = xcorr(voiced_signal[50], voiced_signal[50], maxlags = len(voiced_signal[0])-1)
	# w, z=freqz(framed_signal[65], 1, worN = 2048)
	# plt.plot(sig)
	# plt.figure()

	# plt.plot(framed_signal[65])
	# plt.figure()
	# w = w*fe/(2.*np.pi)
	# plt.plot(w, 20*np.log10(abs(z)))

	# fft=np.fft.ifft(framed_signal[25])
	# freq = np.fft.fftfreq(int(width), 1./fe)
	# plt.plot(freq, abs(fft))

	# tf = fft(framed_signal[25])

	# l1=compute_f0(framed_signal, fe, len(framed_signal[0])-1)
	# l2=compute_f0_cepstrum(framed_signal, fe)
	# for i in range(len(l1)):
	# 	print("l1: ", l1[i], " --- l2: ", l2[i])

	#plot_alot(voice_chooser())
	# plt.show()

	"""frame=framed_signal[200]
	print(len(framed_signal))
	for i in range(len(frame)):
		if frame[i]==0.0:
			print(i)
	k=np.log10(abs(np.asarray(freqz(frame,1,worN=2048))))
	plt.plot(k)
	plt.show()"""